var allTestFiles = [];

var TEST_REGEXP = /(.spec)\.js$/i;

for (var file in window.__karma__.files) {
  if (TEST_REGEXP.test(file)) allTestFiles.push(file);
}

var jsToken = ENV_VARS.getValue('jsToken');
var cssToken= ENV_VARS.getValue('cssToken');
var APPLICATION_INFO = function(){
	this.jsToken 						= jsToken  !== '' ? ENV_VARS.getValue('jsToken').substring(1) + '/'  : '',
	this.cssToken 						= cssToken !== '' ? ENV_VARS.getValue('cssToken').substring(1) + '/' : '';
	this.contextPath 					= ENV_VARS.variables.contextPath;
	this.isInDevelopment 				= ENV_VARS.variables.isInDevelopment;
	this.isInTesting					= ENV_VARS.variables.isInTesting;
	this.isInDevelopmentOrTesting		= ENV_VARS.variables.isInDevelopmentOrTesting,
	this.browserTimeout					= ENV_VARS.variables.browserTimeout;
	this.useMockData					= true;
	this.loadingScriptTimeout			= 5000;
	this.rootPath						= window.rootPath;
	this.domainUrl						= window.domainUrl;
	this.srcPath						= 'devel/';
	this.vendorPath						= 'vendor/';
	this.componentsPath					= 'modules/components/';
	this.sectionsPath					= 'modules/sections/';
	this.templatesPath					= 'templates/';
	this.bootstrapModulesPath			= 'bootstrap_modules/';
	this.assetsCssPath					= 'assets/css/';
	this.dcySdkApiPath					= this.bootstrapModulesPath + 'dcy_sdk_api/';
};

//Configuration Object
var dcyAngular = window.dcyAngular = {
		'SESSION_DATA': {
			'data': {
				'applicationInfo': new APPLICATION_INFO()
			}
		},
		'internalDefine' : window.define //Store define of requirejs in internal variable
};

var paths= {
	angular     					: '/base/environment/vendor/angular/angular',

	angularAnimate					: '/base/environment/vendor/angular-animate/angular-animate.min',
	angularAria						: '/base/environment/vendor/angular-aria/angular-aria.min',
	angularMaterial					: '/base/environment/vendor/angular-material/angular-material',
	ngResource						: '/base/environment/vendor/angular-resource/angular-resource.min',
	ngMocks							: '/base/environment/vendor/angular-mocks/angular-mocks',
	angularSanitize					: '/base/environment/vendor/angular-sanitize/angular-sanitize',
	angularTranslate				: '/base/environment/vendor/angular-translate/angular-translate',


	decisyonConfig					: '/base/environment/devel/config/config',
	env								: '/base/environment/devel/bootstrap_modules/env.provider',
	mockData						: '/base/environment/devel/bootstrap_modules/mock_data.service',
	dp								: '/base/environment/devel/bootstrap_modules/dp.service',
	dcyModule						: '/base/environment/devel/bootstrap_modules/module.provider',

	renderingEnviroment				: '/base/environment/devel/bootstrap_modules/rendering_enviroment.factory',
	dcyService						: '/base/environment/devel/bootstrap_modules/dcy.factory', //Servizio(Factory) esposta all'utente

	dcyRequestErrorHandler			: '/base/environment/devel/bootstrap_modules/request_error_handler.factory',
	dcyToastFactory					: '/base/environment/devel/bootstrap_modules/toast.factory',
	dcyHttpRequest					: '/base/environment/devel/bootstrap_modules/http_request.service',
	dcyHandledEvent					: '/base/environment/devel/bootstrap_modules/handled_event.service',
	dcyUploadRequestService			: '/base/environment/devel/bootstrap_modules/upload_request.service',

	utils							: '/base/environment/devel/bootstrap_modules/utils.factory',
	commonFn						: '/base/environment/devel/bootstrap_modules/commonFn.factory',
	dcyFiltering					: '/base/environment/devel/bootstrap_modules/filtering.filters',
	dcyDialogFn						: '/base/environment/devel/bootstrap_modules/dialog/dialogFn.factory',
	recursionHelper					: '/base/environment/devel/bootstrap_modules/recursion_helper.factory',

	dcyTranslateFilter				: '/base/environment/devel/bootstrap_modules/translate.filter',

	dcyNgRepeatEnd					: '/base/environment/devel/bootstrap_modules/ng_repeat_end.directive',

	templateManagerInterceptor		: '/base/environment/devel/bootstrap_modules/template_manager.factory',
	templateManagerObject			: '/base/environment/devel/bootstrap_modules/template_manager.object',
	templateManager					: '/base/environment/devel/bootstrap_modules/template_manager.directive',
	rightClickManager				: '/base/environment/devel/bootstrap_modules/right_click.directive',

	appCtrl							: '/base/environment/devel/app.controller',
	app								: '/base/environment/devel/app'

};

require.config({
	baseUrl: '/base',
	paths: paths,
	shim: {
			'angular': {
				'exports': 'angular'
			},

			'decisyonConfig': ['angular'],
			'angularAnimate': ['angular', 'decisyonConfig'],
			'angularAria': ['angular', 'decisyonConfig'],
			'angularMaterial': ['angular', 'decisyonConfig'],

			'ngResource': ['angular'],

			'ngMocks': ['angular'],

			//SANITIZE
			'angularSanitize': ['angular'],

			//TRANSLATE
			'angularTranslate': ['angular', 'decisyonConfig'],
			'dcyTranslateFilter': ['angular', 'angularTranslate'],

			//DCY
			'env': ['angular', 'decisyonConfig'],
			'mockData': ['angular', 'decisyonConfig'],
			'dp': ['angular', 'decisyonConfig'],
			'dcyModule': ['angular', 'decisyonConfig'],

			'renderingEnviroment': ['angular', 'decisyonConfig'],
			'dcyService': ['renderingEnviroment', 'angularTranslate'],

			'dcyRequestErrorHandler': ['angular', 'decisyonConfig', 'dcyToastFactory'],
			'dcyToastFactory': ['angular', 'decisyonConfig', 'angularMaterial'],
			'dcyHttpRequest': ['angular', 'decisyonConfig'],
			'dcyHandledEvent': ['angular', 'decisyonConfig'],
			'dcyUploadRequestService': ['angular', 'decisyonConfig'],

			'utils': ['angular', 'decisyonConfig'],
			'commonFn': ['angular', 'decisyonConfig'],

			'recursionHelper': ['angular', 'decisyonConfig'],

			'dcyNgRepeatEnd': ['angular', 'decisyonConfig'],
			'dcyFiltering': ['angular', 'decisyonConfig'],
			'dcyDialogFn': ['angular', 'decisyonConfig'],

			'templateManagerInterceptor': ['angular', 'decisyonConfig'],
			'templateManagerObject': {
				deps: ['angular', 'decisyonConfig'],
				exports: 'templateManagerObject'
			},
			'templateManager': {
				deps: ['angular', 'decisyonConfig', 'templateManagerObject', 'templateManagerInterceptor'],
				exports: 'templateManager'
			},
			'rightClickManager': ['angular', 'templateManager'],
			'app': ['angular', 'decisyonConfig', 'angularMaterial', 'ngResource', 'ngMocks', 'angularSanitize', 'angularTranslate'],
			'appCtrl': ['angular', 'decisyonConfig', 'app']
		},
		priority: ['lodash', 'angular', 'decisyonConfig']
});

require(Object.keys(paths), function(){

	 window.DECISYON = rootPage.DECISYON_CORE.api();
	  /* *********************************************** */

	  window.DECISYON.ng.register = {					
			  controller : function(controller_name, constructor){
				  return  angular.module('dcyApp.controllers').controller(controller_name, constructor);
			  },
			  constant : function(constant_name, constructor){
				  return angular.module('dcyApp.constants').constant(controller_name, constructor);
			  },
			  provider : function(provider_name, constructor){
				  return angular.module('dcyApp.providers').provider(controller_name, constructor);
			  },
			  service : function(service_name, constructor){
				  return angular.module('dcyApp.services').service(controller_name, constructor);
			  },
			  filter : function(filter_name, constructor){
				  return angular.module('dcyApp.filters').filter(controller_name, constructor);
			  },
			  factory : function(factory_name, constructor){
				  return angular.module('dcyApp.factories').factory(controller_name, constructor);
			  },
			  directive : function(directive_name, constructor){
				  return angular.module('dcyApp.directives').directive(controller_name, constructor);
			  },
			  module : function(module_name){
				  console.log('REGISTER MODULE API IS NOT SUPPORTED FOR UNIT TEST! USE JASMINE API module(...)!');
			  }
	  };

	  window.DECISYON.alpha.addWatchToParam = function(window, paramName, callback){
		  try{
			  if(typeof window !== 'object' || typeof callback !== 'function' || typeof window.paramsWatched[paramName] === 'undefined'){
				  console.error('[addWatchToParam] => The parameters are not valid for attach the listener on change event.');
				  return false;
			  }
			  window.paramsWatched.dcy_watch(paramName,function(keyToChange, oldValue, newValue) {
				  callback(keyToChange, oldValue, newValue);
			  });
			  return true;
		  }
		  catch(e){
			  console.error('[addWatchToParam] => This Api is not avaible in this context.It is avaible only on mashboard.');
			  return false;
		  }
	  };

	setTimeout(function(){
		require(allTestFiles, function(){
			window.__karma__.start();
		});	
	});
	
});
