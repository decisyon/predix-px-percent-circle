(function() {
	'use strict';
	
	/**
	* Controller to manage the percent circle widget.
	* link: https://www.predix-ui.com/?show=px-percent-circle&type=component
	*/
	function PxPercentCircleCtrl($scope, $timeout, $element){
		var ctrl								= this;
		var	ctx								    = $scope.DECISYON.target.content.ctx;
		var	refObjId							= ctx.refObjId.value;
		var	EMPTY_PARAM_VALUE					= 'PARAM_VALUE_NOT_FOUND';
		var	unbindWatches						= [];
		var OVERFLOW_BUG_FIX                    = 10;
		var wdgPercentCircle;

		/* Managing of line chart Properties */
		var manageParamsFromContext = function(context) {
	        var maxContent		    = context.$pxPercentCircleMax.value,
		        valContent		    = context.$pxPercentCircleVal.value,
		        thicknessContent	= context.$pxPercentCircleThickness.value;
		        
		    ctrl.max	    = (maxContent !== EMPTY_PARAM_VALUE && maxContent !== '') ?  maxContent : DEFAULT_MAX;
		    ctrl.val	    = (valContent !== EMPTY_PARAM_VALUE && valContent !== '') ? valContent : DEFAULT_VAL;
		    ctrl.thickness	= (thicknessContent !== EMPTY_PARAM_VALUE && thicknessContent !== '') ? thicknessContent : DEFAULT_THICKNESS; 
		};
        
        /* Replace apostrophe unicode - bug to fixed in json object */
		var replaceApostropheUnicode  = function(value) {
			return value.replace('\\u0027', '');
		};
		
        /* Pick the RGB color */
		var checkRGBColor = function(value) {
			var espr = /^rgb\((0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),(0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d),(0|255|25[0-4]|2[0-4]\d|1\d\d|0?\d?\d)\);?$/;
			if (espr.test(value)) {
				return value;
			}
			return '';
		};
		
        /* Management of percent circle styling */
		var manageStylingProperty = function(stylingProp) {
			var stylingPropValue = checkRGBColor(replaceApostropheUnicode(stylingProp.value));
			return (stylingPropValue !== EMPTY_PARAM_VALUE) ? stylingPropValue : '';
		};
		
        /* Management of percent circle styling by reading custom properties settings */
		var manageCustomStyling = function(ctx) {
			wdgPercentCircle.customStyle['--px-percent-circle-background-color']    = manageStylingProperty(ctx.$pxPercentCircleBGColor);
			wdgPercentCircle.customStyle['--px-percent-circle-fill-color']          = manageStylingProperty(ctx.$pxPercentCircleFillColor);
			wdgPercentCircle.customStyle['--px-percent-circle-text-color']          = manageStylingProperty(ctx.$pxPercentCircleTextColor);
			wdgPercentCircle.updateStyles();
		};
		
		/* Inizialize chart*/
		var inizializeChart = function(ctx) {
			manageParamsFromContext(ctx);
			manageCustomStyling(ctx);
			attachListeners(ctx);
			applyDimensionsToWebComponent(ctx);
		};
		
		/* Update chart for context parameters and dimesion*/
		var updateChart = function(ctx) {
			manageParamsFromContext(ctx);
			manageCustomStyling(ctx);
		};
		
		/* Routine called when angular element is destroied*/
		var destroyWidget = function() {			
			for (var i = 0; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};
		
		/* It adjust size ad fix overflow*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace(/%/g, '').trim();
			return (adjustedSize > OVERFLOW_BUG_FIX) ? (adjustedSize - OVERFLOW_BUG_FIX) : adjustedSize;
		};
		
		/* 	Set the container widget size.If you set both this and height to "auto", the chart will expand to fill its containing element.
			Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		*/
		var applyDimensionsToWebComponent = function(ctx) {
			ctrl.width    = getAdjustedSizing(ctx['min-width'].value);
			ctrl.height   = getAdjustedSizing(ctx['min-height'].value);
		};
		
		/* Watch on widget context to observe parameter changing after loading */
		var attachListeners = function() {
			unbindWatches.push($scope.$watch('DECISYON.target.content.ctx', function(newCtx, oldCtx) {
				if (!angular.equals(newCtx, oldCtx)){
					updateChart(newCtx);
				}
			}, true));
			//Listener on destroy angular element; in this case we destroy all watch and listener
			$scope.$on('$destroy', destroyWidget);
		};
		
		ctrl.setPredixPercentCircleLibraryLoaded = function() {
		    wdgPercentCircle       = $('px-percent-circle', $element)[0];
			if (wdgPercentCircle){
		        inizializeChart(ctx);	
			}
		};
		//Inzitialize
	}
	PxPercentCircleCtrl.$inject = ['$scope', '$timeout', '$element'];

	DECISYON.ng.register.controller('pxPercentCirclePredixCtrl', PxPercentCircleCtrl);
}());