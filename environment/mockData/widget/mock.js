var rootPath                  = '../..';
var resourcesFolderPath       = '/resources';
var resourcesPath             = rootPath + resourcesFolderPath;
var mockpreviewPath           = resourcesPath  + '/metadata/mock/mock_preview/';
var mockContextNameResources  = mockpreviewPath + 'mockContext.json';
var mockDataNameResources     = mockpreviewPath + 'mockData.json';
var previewMappingJson        = 'environment.json';

var getNormalizeResponse = function(results){
  var normalized = {
    view : 'index.html',
    basePath : resourcesFolderPath
  };
  _.forEach(results, function(result) {
    if(result.config.url.lastIndexOf(mockDataNameResources) != -1){
      normalized.data = result.data;
    }else if(result.config.url.lastIndexOf(mockContextNameResources) != -1){
      normalized.ctx = result.data;
    }else if(result.config.url.lastIndexOf(previewMappingJson) != -1){
      normalized.deps = result.data.deps;
      //TODO FRA : Quando implementeremo la feature di sync con mock/ctx remoto dovremmo prendere in considerazione il basePath che mi arriva
    }
  });

  return normalized;
}

function getMockInformations() {
  var $http = invokeAngularService('$http');
  var $q    = invokeAngularService('$q');

  var deferer = $q.defer();
  var promises = [];

  var mockResources = (mockContextNameResources +','+ mockDataNameResources + ',' + previewMappingJson).split(',');

  _.forEach(mockResources, function(mockResource) {
    promises.push($http.get(mockResource));
  });

  $q.all(promises).then(function(results){
    deferer.resolve(getNormalizeResponse(results));
  },function(rejects){
    deferer.resolve(rejects);
  });

  return deferer.promise;
}

function getData(params) {
  return getMockInformations();
}
