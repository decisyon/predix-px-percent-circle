 (function() {
   'use strict';


   function AppController($rootScope, $scope, $log) {
	   
	  $log.info('App Controller STARTED!!!');
	  
	  $scope.evaluateRefreshPreview = function($event){
		  if($event.keyCode == 116){//f5
			  $event.preventDefault();
			  window.location.replace(window.location.href);
		  }
	  };

   }
   
   
   
   AppController.$inject = ['$rootScope', '$scope', '$log'];
   
   /**
    * define angular injections
    */
   

   angular
     .module('dcyApp.controllers')
     .controller('dcyAppController', AppController);

 }());
